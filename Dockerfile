FROM openjdk:11-jre

ADD ./build/distributions/devops-0.0.1.tar /opt/

CMD ["/opt/devops-0.0.1/bin/devops"]
